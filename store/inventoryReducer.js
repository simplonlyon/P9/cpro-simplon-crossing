import { createSlice } from "@reduxjs/toolkit";
import { TILE_TYPE } from "../components/Tile";
import * as SecureStore from 'expo-secure-store';
import { emptyTile, assignBuildings } from "./gridReducer";
import { Platform } from "react-native";
import { select, close } from "./contextMenuReducer";




const inventorySlice = createSlice({
    name: 'inventory',
    initialState: {
        resources: {
            wood: 0,
            rock: 0,
            water: 0,
            wheat: 0
        },
        buildings: []
    },
    reducers: {
        addResource(state, {payload}) {
            switch (payload) {
                case TILE_TYPE.WOOD:
                    state.resources.wood++;
                    break;
                case TILE_TYPE.ROCK:
                    state.resources.rock++;
                    break;
                case TILE_TYPE.WATER:
                    state.resources.water++;
                    break;
                case TILE_TYPE.WHEAT:
                    state.resources.wheat++;
                    break;
            }
        },
        setResources(state, {payload}) {
            state.resources = payload;
        },
        addBuilding(state, {payload}) {
            if(!state.buildings[payload.x]) {
                state.buildings[payload.x] = [];
            }
            state.buildings[payload.x][payload.y] = payload.building;
        },
        setBuildings(state, {payload}) {
            state.buildings = payload;
        }
    }
});

export const {addResource, setResources, addBuilding, setBuildings} = inventorySlice.actions;
export const inventoryReducer = inventorySlice.reducer;

export const getResources = () => async (dispatch) => {
    let stored;
    let buildings;

    if(Platform.OS === 'web') {
        stored = localStorage.getItem('resources');
        buildings = localStorage.getItem('buildings');
    } else {
        stored = await SecureStore.getItemAsync('resources');
        buildings = await SecureStore.getItemAsync('buildings');
    }

    if(stored) {
        dispatch(setResources({wood:0, rock:0, water: 0, wheat:0, ...JSON.parse(stored)}));
    }
    if(buildings) {
        await dispatch(setBuildings(JSON.parse(buildings)));
        dispatch(assignBuildings());
    }

}

export const collectResource = () => async (dispatch, getState) => {
    const selected = getState().contextMenu.selected;

    await dispatch(emptyTile(selected));

    await dispatch(addResource(selected.type));

    dispatch(select({x:selected.x, y:selected.y, type: TILE_TYPE.EMPTY}))
    
    if(Platform.OS === 'web') {
        localStorage.setItem('resources', JSON.stringify(getState().inventory.resources));
        localStorage.setItem('grid', JSON.stringify(getState().grid));
    } else {
        await SecureStore.setItemAsync('resources', JSON.stringify(getState().inventory.resources));
        await SecureStore.setItemAsync('grid', JSON.stringify(getState().grid));
    }
}

export const build = (building) => async (dispatch, getState) => {

    const selected = getState().contextMenu.selected;
    const resources = getState().inventory.resources;

    if(selected) {
        await dispatch(addBuilding({x: selected.x, y: selected.y, building}));
        await dispatch(setResources({wood: resources.wood - building.wood, rock: resources.rock - building.rock, water: resources.water - building.water}));
        dispatch(close());
        dispatch(assignBuildings());
        if(Platform.OS === 'web') {
            localStorage.setItem('resources', JSON.stringify(getState().inventory.resources));
            localStorage.setItem('buildings', JSON.stringify(getState().inventory.buildings));
        } else {
            await SecureStore.setItemAsync('resources', JSON.stringify(getState().inventory.resources));
            await SecureStore.setItemAsync('buildings', JSON.stringify(getState().inventory.buildings));
        }
        
    }
};

