import { createSlice } from "@reduxjs/toolkit";
import { TILE_TYPE } from "../components/Tile";
import * as SecureStore from 'expo-secure-store';
import { Platform } from "react-native";

const gridSlice = createSlice({
    name: 'grid',
    initialState: {
        grid: [],
        expireAt: 0
    },
    reducers: {
        change(state, {payload}) {
            state.grid = payload;
        },
        replace(state, {payload}) {
            state.grid = payload.grid;
            state.expireAt = payload.expireAt;
            
        },
        emptyTile(state, {payload}) {
            state.grid[payload.x][payload.y] = TILE_TYPE.EMPTY;
        },
        changeTile(state, {payload}) {
            state.grid[payload.x][payload.y] = payload.type;
        }
    }
});

export const {change, replace, emptyTile, changeTile} = gridSlice.actions;

export const gridReducer = gridSlice.reducer;


export const generateGrid = (size = 20) => async (dispatch) => {
    
    let grid = [];
    let stored;
    if(Platform.OS === 'web') {
        stored = localStorage.getItem('grid');
    } else {
        stored = await SecureStore.getItemAsync('grid');
    }
    
    if(stored) {        
       stored = JSON.parse(stored);
       if(stored.expireAt > Date.now()) {
             return dispatch(replace(stored));
       }
    }
 

    for (let x = 0; x < size; x++) {
        grid[x] = [];
        for(let y = 0; y < size; y++) {
            let type = TILE_TYPE.EMPTY;
            let random = Math.random();
            if(random > 0.5 && random < 0.75) {
                type = TILE_TYPE.WOOD;
            }
            if(random > 0.75) {
                type = TILE_TYPE.ROCK;
            }
            grid[x][y] = type;
        }    
    }
    const newState = {
        grid, 
        expireAt: Date.now() + 86400000
    };
    if(Platform.OS === 'web') {
        localStorage.setItem('grid', JSON.stringify(newState));
    } else {

        await SecureStore.setItemAsync('grid', JSON.stringify(newState));
    }
    
    return dispatch(replace(newState));
    
}

export const assignBuildings = () => async (dispatch, getState ) => {
    
    const buildings = getState().inventory.buildings;

    for(let x = 0; x < buildings.length; x++) {
        if(buildings[x]) {
            for(let y = 0; y < buildings[x].length; y++) {
                if(buildings[x][y]) {
                    for(let bx = x; bx < x+buildings[x][y].width; bx++) {
                        for(let by = y; by < y+buildings[x][y].height; by++) {
                            
                            dispatch(changeTile({x: bx, y: by, type: TILE_TYPE.BUILDING}));
                        }
                    }
                }
            }
        }
    }

}