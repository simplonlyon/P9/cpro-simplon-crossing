import { createSlice } from "@reduxjs/toolkit";

export const MENU_TYPE = {
    DEFAULT: 'DEFAULT',
    TILE_ACTION: 'TILE_ACTION',
    BUILD: 'BUILD',
    BUILDING_ACTION: 'BUILDING_ACTION'
};


const contextMenuSlice = createSlice({
    name: 'contextMenu',
    initialState: {
        selected: null,
        isOpen: false,
        menuType: MENU_TYPE.DEFAULT
    },
    reducers: {
        select(state, action) {
            state.selected = action.payload;
            state.isOpen = true;
            if(action.payload.building) {
                state.menuType = MENU_TYPE.BUILDING_ACTION;
            } else {
                state.menuType = MENU_TYPE.TILE_ACTION;
            }
            
        },
        close(state, action) {
            state.isOpen = false;
        },
        changeMenu(state, {payload}) {
            state.menuType = payload;
        }
    }
});

export const {select, close, changeMenu} = contextMenuSlice.actions;

export const contextMenuReducer = contextMenuSlice.reducer;