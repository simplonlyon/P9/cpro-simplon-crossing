import React from 'react';
import { configureStore } from '@reduxjs/toolkit';
import { contextMenuReducer } from './store/contextMenuReducer';
import { Provider } from 'react-redux';
import MainScreen from './screen/MainScreen';
import { gridReducer, generateGrid } from './store/gridReducer';
import { getResources, inventoryReducer } from './store/inventoryReducer';

const store = configureStore({
  reducer: {
    contextMenu: contextMenuReducer,
    grid: gridReducer,
    inventory: inventoryReducer
  }
});

store.dispatch(generateGrid());
store.dispatch(getResources());

export default function App() {
  return (
    <Provider store={store}>
      <MainScreen />
    </Provider>
  );
}
