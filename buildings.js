import { addResource } from './store/inventoryReducer';
import { TILE_TYPE } from './components/Tile';
import { Platform } from 'react-native';
import * as SecureStore from 'expo-secure-store';


export const buildings = [
    {id: 1, name: 'Well', wood: 0, rock: 3, width: 1, height: 1, asset: require('./assets/well.png'), actions: [
        {name: 'Draw Water', cooldown: 10000, effect: 'drawWater'}
    ]},
    {id: 2, name: 'Table', wood: 2, rock: 0, width: 1, height: 1, asset: require('./assets/table.png'), actions: []},
    {id: 3, name: 'House', wood: 4, rock: 4, width: 2, height: 2, asset: require('./assets/house.png'), actions: []},
    {id: 4, name: 'Wheat', wood: 0, rock: 0, water: 2, width: 1, height: 1, asset: require('./assets/wheat.png'), actions: [
        {name: 'Harvest', cooldown: 60000, effect: 'harvestWheat'}
    ]},

];

export const buildingActions= {
    drawWater: () => async (dispatch, getState) => {
        await dispatch(addResource(TILE_TYPE.WATER));
        if(Platform.OS === 'web') {
            localStorage.setItem('resources', JSON.stringify(getState().inventory.resources));
        } else {
            SecureStore.setItemAsync('resources', JSON.stringify(getState().inventory.resources));
        }
    },
    harvestWheat: () => async (dispatch, getState) => {
        await dispatch(addResource(TILE_TYPE.WHEAT));
        if(Platform.OS === 'web') {
            localStorage.setItem('resources', JSON.stringify(getState().inventory.resources));
        } else {
            SecureStore.setItemAsync('resources', JSON.stringify(getState().inventory.resources));
        }
    }
} ;