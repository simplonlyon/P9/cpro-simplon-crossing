import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { close, MENU_TYPE } from '../store/contextMenuReducer';
import TileMenu from './TileMenu';
import BuildMenu from './BuildMenu';
import ActionMenu from './ActionMenu';


export default function ContextMenu() {

    const dispatch = useDispatch();
    const menuType = useSelector(state => state.contextMenu.menuType);

    const displayMenu = () => {
        if(menuType === MENU_TYPE.TILE_ACTION) {
            return <TileMenu />
        }
        if(menuType === MENU_TYPE.BUILD) {
            return <BuildMenu />
        }
        if(menuType === MENU_TYPE.BUILDING_ACTION) {
            return <ActionMenu />
        }
    }

    return (
        <View style={styles.menu}>
            <View style={styles.closeBtn}>
                <Button title="close" onPress={() => dispatch(close())} />
            </View>
            {displayMenu()}
        </View>
    );
}



const styles = StyleSheet.create({
    menu: {
        padding: 20,
        height: 200,
        width: '100%',
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0
    },
    closeBtn: {
        position: 'absolute',
        top: 5,
        right: 5,
        width: 60
    }
});