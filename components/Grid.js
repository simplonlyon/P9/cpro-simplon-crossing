import React from 'react';
import { View, Image } from 'react-native';
import Tile from './Tile';
import { useSelector } from 'react-redux';
import TileBuilding from './TileBuilding';


export default function Grid() {


    const grid = useSelector(state => state.grid.grid);
    const buildings = useSelector(state => state.inventory.buildings);


    return (
        <View>
            {grid.map((line, x) => (
                <View style={{flexDirection: 'row', zIndex:1}} key={x}>
                    {line.map((type, y) => <Tile key={y} x={x} y={y} type={type} />)}    
                </View>
            ))}

        {buildings.map((line, x) => (
            line && line.map((building, y) => (
                building && <TileBuilding  key={x+''+y} x={x} y={y} building={building} />

            ))
        ))
            }
        </View>
    );
}