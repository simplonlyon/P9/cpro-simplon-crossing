import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';
import { select } from '../store/contextMenuReducer';


export default function TileBuilding ({building, x, y}) {
    const dispatch = useDispatch();
    return (
        <TouchableOpacity onPress={() => dispatch(select({x,y,building}))} style={{width:building.width*50, height:building.height*50, position:'absolute', top: x*50, left: y*50, zIndex:10, backgroundColor: 'white'}}>
            <Image source={building.asset} style={{width:'100%', height: '100%'}}/>
        </TouchableOpacity>
    );
}