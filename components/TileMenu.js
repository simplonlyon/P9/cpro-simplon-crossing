import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, Text, Button } from "react-native";
import { TILE_TYPE } from "./Tile";

import { collectResource } from "../store/inventoryReducer";
import { changeMenu, MENU_TYPE } from "../store/contextMenuReducer";

export default function TileMenu() {
    const dispatch = useDispatch();
    const selectedTile = useSelector(state => state.contextMenu.selected);
    const resources = useSelector(state => state.inventory.resources);
    return (
        <View>
            <Text>You selected tile x: {selectedTile.x}, y: {selectedTile.y}</Text>
            <Text>wood: {resources.wood} rock: {resources.rock} water: {resources.water}</Text>
                <View style={{width:250, marginTop: 20}}>
                    {selectedTile.type !== TILE_TYPE.EMPTY && selectedTile.type !== TILE_TYPE.BUILDING && (
                    <Button title="Collect" onPress={() => dispatch(collectResource())}></Button>
                    )}
                    {selectedTile.type === TILE_TYPE.EMPTY && (
                        <Button title="Build" onPress={() => dispatch(changeMenu(MENU_TYPE.BUILD))}></Button>
                    )}
                </View>
        </View>
    );
}