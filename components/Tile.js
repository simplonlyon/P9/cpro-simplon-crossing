import React from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { select } from '../store/contextMenuReducer';

export const TILE_TYPE = {
    EMPTY: 'EMPTY',
    WOOD: 'WOOD',
    ROCK: 'ROCK',
    BUILDING: 'BUILDING',
    WATER: 'WATER',
    WHEAT: 'WHEAT'
};


export default function Tile({ x, y, type = TILE_TYPE.EMPTY }) {

    const dispatch = useDispatch();
    const selected = useSelector(state => state.contextMenu.selected);
    const buildings = useSelector(state => state.inventory.buildings);

    const tapTile = () => {
        if(buildings[x] && buildings[x][y]) {
            dispatch(select({ x, y, type: TILE_TYPE.BUILDING }));

        }else {
            dispatch(select({ x, y, type }));

        }
    }

    const styleType = () => {
        if (type === TILE_TYPE.EMPTY) {
            return;
        }
        if (type === TILE_TYPE.ROCK) {
            return <Image style={{width: '100%', height:'100%'}} source={require('../assets/rock.png')} />
        }
        if (type === TILE_TYPE.WOOD) {
            return <Image style={{width: '100%', height:'100%'}} source={require('../assets/tree.png')} />
        }
    }

    const isSelected = () => {
        if (selected) {
            if (x === selected.x && y === selected.y) {
                return styles.highlight;
            }
        }
    }

    return (
        <TouchableOpacity style={[styles.tile, isSelected()]} onPress={tapTile} >
            {styleType()}
        </TouchableOpacity>
    );
}


const styles = StyleSheet.create({
    tile: {
        width: 50,
        height: 50,
        borderColor: 'black',
        borderWidth: 1,
        zIndex: 0
    },
    highlight: {
        borderColor: 'yellow'
        
    }
});