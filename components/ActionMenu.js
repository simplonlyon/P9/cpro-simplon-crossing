import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, Text, Button } from "react-native";
import { buildings, buildingActions } from "../buildings";


export default function ActionMenu() {
    const dispatch = useDispatch();
    const selectedTile = useSelector(state => state.contextMenu.selected);
    
    const building = buildings.find(item => selectedTile.building.id === item.id);
    return (
        <View>
            <Text>What do you want to do with your {building.name} ?</Text>
            {building.actions.map(action => (
            <View key={action.name} style={{width: 150}}>
                <Button title={action.name} onPress={() => dispatch(buildingActions[action.effect]())}/>
            </View>
            )
            )}
        </View>
    );
}