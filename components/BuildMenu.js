import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, Button, Text } from "react-native";
import { buildings } from "../buildings";
import { build } from "../store/inventoryReducer";
import { TILE_TYPE } from "./Tile";



export default function BuildMenu() {
    const dispatch = useDispatch();
    const resources = useSelector(state => state.inventory.resources);
    const grid = useSelector(state => state.grid.grid);
    const selected = useSelector(state => state.contextMenu.selected);
    

    const isDisabled = (building) => {
        if(resources.wood < building.wood || resources.rock < building.rock || resources.water < buildings.water) {
            return true;
        }
        for(let x = selected.x; x < selected.x + building.width; x++)     {
            if(!grid[x]) {
                return true;
            }
            for(let y = selected.y; y < selected.y + building.height; y++) {
                if(grid[x][y] !== TILE_TYPE.EMPTY) {
                    return true;
                }
            }
        }
        return false;
    }
    
    return (
        <>
        <Text>Choose what to build</Text>
        <View style={{flexDirection:'row', flexWrap: 'wrap'}}>
            {buildings.map(building => (
                <View key={building.id} style={{width:200, margin: 5}}>
                    <Button onPress={() => dispatch(build(building))} disabled={isDisabled(building)} title={building.name}></Button>
                </View>
            ))}
            
        </View>
        </>
    );
}