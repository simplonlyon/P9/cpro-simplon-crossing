import React from 'react';
import { ScrollView, View } from 'react-native';
import Grid from '../components/Grid';
import ContextMenu from '../components/ContextMenu';
import { useSelector } from 'react-redux';


export default function MainScreen() {
    const isOpen = useSelector(state => state.contextMenu.isOpen);



  return (
      <View>
        <ScrollView >
          <ScrollView horizontal={true}>

            <Grid />
          </ScrollView>
        </ScrollView>
        {isOpen && <ContextMenu />}
      </View>
  );
}
